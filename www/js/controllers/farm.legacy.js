angular.module('originsApp.controllers.farm')

.controller('FarmSaveController.legacy', function($scope, $stateParams, Storage, $http, $window, $state, SERVICE_CONFIG, $ionicPlatform, my_alert) {
    
    $scope.farm = {};
    $scope.farm.legacy = {};
    $scope.farm.legacy.audio = {};

    $scope.status = 'waiting';
    
    $scope.audio_src;

    var farm_id = $scope.farm_id = $stateParams.farm_id;

    var mediaRec;
   
    
    Storage.get($scope.farm_id).then(function(response) {
        $scope.farm = response;
        if (response.legacy == undefined) $scope.farm.legacy = {};
        if (response.legacy.audio == undefined) $scope.farm.legacy.audio = {};
    });

   
    $scope.captureAudio = function() {

        $ionicPlatform.ready(function() {

            var index = Object.keys($scope.farm.legacy.audio).length;

            if ($scope.status == 'waiting') {

                var src = $scope.audio_src = "documents://" + farm_id + "-legacy-" + index + '.wav'; 
                
                mediaRec = new Media(src,
                    // success callback
                    function() {
                       // console.log("recordAudio():Audio Success");
                    },

                    // error callback
                    function(err) {
                       // console.log("recordAudio():Audio Error: "+ err.code);
                });

                // Record audio
                mediaRec.startRecord();

                $scope.status = "recording";

            }
            else if ($scope.status == 'recording') {

                mediaRec.stopRecord();
                $scope.farm.legacy.audio[index] = $scope.audio_src;
                mediaRec.release();

                $scope.status = 'waiting';
            }
        });
    }

    $scope.play_audio = false;
    $scope.playing = {};
    $scope.currently_playing;

    $scope.playAudio = function(index) {

        if ($scope.status == 'recording') return;
        if ($scope.status == 'playing' && $scope.currently_playing != index ) return;

        if ($scope.play_audio) {
            $scope.play_audio.stop();
            $scope.play_audio.release();
            $scope.play_audio = false;
            $scope.status = 'waiting';
            $scope.playing[index] = 0;
            $scope.currently_playing = 0;

            return;
        }

        $ionicPlatform.ready(function() {

            var src = $scope.farm.legacy.audio[index];

            $scope.play_audio = new Media(src,
                // success callback
                function() {
                    //console.log("playAudio():Audio Success");
                },
                // error callback
                function(err) {
                   // console.log("playAudio():Audio Error: "+err);
                }
            );

            $scope.play_audio.play();
            $scope.status = 'playing';
            $scope.playing[index] = 1;
            $scope.currently_playing = index;
           
        });
    }

    $scope.removeAudio = function(id) {
        if ($scope.playing[id] == 1) $scope.playAudio(id);
        delete $scope.farm.legacy.audio[id];
    }
    
        
    var next = false;
    $scope.legacySubmit = function(next) {
        $scope.farm_id = $stateParams.farm_id;        
        go_next = false;     
        if (next) next = true;
        Storage.update($scope.farm_id, $scope.farm.legacy, 'legacy').then(function() {
            if (next) $state.go('farm-save.plans');
            else my_alert.show('Data Saved Successfully.');
        });
    }
})
