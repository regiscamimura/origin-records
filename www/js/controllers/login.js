angular.module('originsApp.controllers.login', [])

.controller('LoginController', function ($scope, Auth, Storage, $state, $rootScope, $timeout) {
    
    $scope.rec = {};
    
    $scope.status = "";
    $scope.message = "";
    
    $scope.submit = function() {
        $rootScope.$broadcast('loading:show');
        Auth.login($scope.rec).success(function(response) {
            $rootScope.$broadcast('loading:hide');
            $scope.status = response.status;
            if ($scope.status == 'error') {
                $scope.message = "Invalid username/password.";
            }
            else {
                Storage.create();
                $scope.message = "You are now connected to your web account!";
                $rootScope.user_id = Auth.user_id();
                $rootScope.name = Auth.name();
                $timeout(function() {
                    $state.go("index");
                }, 1000);
            }
        });
    }   

})
