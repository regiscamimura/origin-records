angular.module('originsApp.controllers.farm')

.controller('FarmSaveController.varieties', function($scope, $ionicPopup, $stateParams, Storage, $http, $window, $state, SERVICE_CONFIG, my_alert) {
    
    $scope.farm = {};
    $scope.farm.variety = {0: ''};
    $scope.farm_id = $stateParams.farm_id;
       
    $http.get('js/data/varieties.json').success(function(data){
        $scope.variety_listing = data;
    });
    
    $scope.addVariety = function() {
        index = Object.keys($scope.farm.variety).length;
        $scope.farm.variety[index] = ""; 
    };
    $scope.removeVariety = function(index) {
        delete $scope.farm.variety[index];
    };
    
    
    Storage.get($scope.farm_id).then(function(response) {
        $scope.farm = response; 
        if (response.variety == undefined) $scope.farm.variety = {0: ''};
    });
    
    
    var next = false;
    $scope.varietySubmit = function(next) {
        $scope.farm_id = $stateParams.farm_id;        
        go_next = false;     
        if (next) next = true;
        Storage.update($scope.farm_id, $scope.farm.variety, 'variety').then(function() {
            if (next) $state.go('farm-save.processing');
            else my_alert.show('Data Saved Successfully.');
        });
    };    
    
});
