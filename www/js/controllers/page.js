angular.module('originsApp.controllers.page', [])

.controller('PageController', function ($scope, $window, $timeout) {
    
    angular.element($window).on('resize', function() {
        $scope.img_size();
    });
    angular.element($window).on('load', function() {
        $scope.img_size();
    });

    $scope.img_size = function() {
        
        
        var height = $scope.height = window.innerHeight - angular.element(document.querySelector('.ci-footer')).prop('offsetHeight') - angular.element(document.querySelector('.bar-header')).prop('offsetHeight') - angular.element(document.querySelector('h2.ci-sub-header')).prop('offsetHeight');
        var width = window.innerWidth;

        console.log(width);
        console.log(height);
        
        if (width > height) {
            $scope.width = "100%";
            $scope.height = "auto";
        }
        else {
            $scope.width = "auto";
            $scope.height = height+"px";

            
            $timeout(function() {
                var img = angular.element(document.querySelector('#page-image'));
                var page_scroller = angular.element(document.querySelector('.page-scroller'));

                img.on('load', function() {                  
                    page_scroller[0].scrollLeft = ( img.prop('width') - page_scroller.prop('offsetWidth') ) / 2;
                })
                
            }, 10);
        }
    }

})
