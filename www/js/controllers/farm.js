angular.module('originsApp.controllers.farm', [])

.controller('FarmIndexController', function ($scope, Storage, $ionicPopup) {
     Storage.listing().then(function(response) {
         $scope.listing = response; 
     });

    $scope.delete_record = function(id) {
        $ionicPopup.show({
            title: '<h3>Delete Record</h3>',
            template: 'Are you sure you want to delete this record?',
            cssClass: 'ci-popup',
            scope: $scope,
            buttons: [
                { 
                    text: 'Cancel',
                    type: 'button-default',
                    onTap: function(e) {
                       
                    }
                }, 
                {
                    text: 'OK',
                    type: 'button-positive',
                    onTap: function(e) {
                        Storage.delete_record(id).then(function(response){
                            delete $scope.listing[id];
                        });
                    }
                }
            ]
        });
        
    }

})

.controller('FarmSaveController', function($scope, $stateParams, $state, Storage, $timeout, $window) {
    $scope.farm_id = $stateParams.farm_id;
    $scope.edit_mode = false;
    
    Storage.get($scope.farm_id).then(function(response) {
        $scope.farm = response;
    }, function(error) {
        alert(error);
        $state.go('index');        
    });
    
    $scope.edit = function() {
        if ($scope.edit_mode) $scope.edit_mode = false;
        else {
            $scope.edit_mode = true;
            $window.document.getElementById('farm-name-input').focus();
        }
    }
    
    var update_to;
    var update_field;
    $scope.update = function(field) {
        if (update_to) $timeout.cancel(update_to);
        update_field = field;
    
        update_to = $timeout(function() {
            var data = {};
            data[update_field] = $scope.farm[update_field];
            Storage.update($scope.farm_id, data);
        }, 1000);
    }
    
})

.controller('FarmSaveController.main', function($scope, $stateParams, Storage) {
    $scope.farm_id = $stateParams.farm_id;
    
    
})
