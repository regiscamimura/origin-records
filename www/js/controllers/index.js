angular.module('originsApp.controllers.index', [])

.controller('IndexController', function($rootScope, $scope, $ionicPopup, $state, Auth, Storage, $ionicModal, $ionicPlatform, $cordovaCamera, $cordovaGeolocation, $timeout, $window) {

	$scope.farm = {};

	$scope.listing = [];
	Storage.listing().then(function(response) {
		for (var i in response) {
			$scope.listing.push(response[i]);
		}
	});

	$rootScope.user_id = Auth.user_id();
	$rootScope.name = Auth.name();


	$scope.openPopup = function() {
		$ionicPopup.show({
			title:       '<h3>Create a New Record</h3>',
			templateUrl: 'views/farm/add-popup.html',
			cssClass:    'ci-popup',
			scope:       $scope,
			buttons:     [
				{
					text:  'Cancel',
					type:  'button-default',
					onTap: function(e) {

					}
				},
				{
					text:  'OK',
					type:  'button-positive',
					onTap: function(e) {
						return $scope.submit();
					}
				}
			]
		});
	};

	$scope.submit = function() {
		Storage.add($scope.farm).then(function(response) {
			$state.go("farm-save.main", {"farm_id": response.id});
		});
	};

	// Postcard
	$scope.capturePhoto = function() {

		$scope.postcard = {};

		$cordovaGeolocation.getCurrentPosition({timeout: 20000, enableHighAccuracy: true})
		.then(function(position) {
			$scope.postcard.lat = position.coords.latitude;
			$scope.postcard.lng = position.coords.longitude;
			$scope.postcard.alt = Math.round(position.coords.altitude);

		}, function(error) {
			console.log(error);
		});

		if (window.plugin && window.plugin.statusbarOverlay) {
			window.plugin.statusbarOverlay.hide();
		}

		$ionicPlatform.ready(function() {
			var options = {
				quality:          50,
				destinationType:  Camera.DestinationType.FILE_URI,
				sourceType:       Camera.PictureSourceType.CAMERA,
				allowEdit:        false,
				encodingType:     Camera.EncodingType.JPEG,
				targetWidth:      1136,
				targetHeight:     640,
				popoverOptions:   CameraPopoverOptions,
				saveToPhotoAlbum: false
			};

			$cordovaCamera.getPicture(options).then(
				function(imageUri) {

					if (window.plugin && window.plugin.statusbarOverlay) {
						window.plugin.statusbarOverlay.hide();
					}

					//Storage.add({'image': imageUri}, 'postcard');
					$scope.postcard.img = imageUri;
					$scope.$apply();
					$scope.postcardModal();


				},
				function(err) {
					// error
				}
			);

		});
	}


	 $ionicModal.fromTemplateUrl('views/postcard/modal.html', {
		 scope: $scope,
		 animation: 'slide-in-up'
	 })
	 .then(function(modal) {
		 $scope.modal = modal;
	 });

	 $scope.postcardModal = function() {
	 	$scope.modal.show();
		$timeout(function() {
			$scope.resetPostcard();
		}, 500);
	 };

	$scope.resetPostcard = function() {
		wmark.init({
			"target":    "watermark", // default "watermark"
			"watermark": "img/watermark.png"
		});
	}

	$scope.postcardSave = function() {

		window.canvas2ImagePlugin.saveImageDataToLibrary(
			function(img) {
				var path = cordova.file.documentsDirectory.replace("file://", "");
				$scope.postcard.img = img.replace(path, "");

				Storage.add($scope.postcard, 'postcard');
				$scope.postcard = {};

				var els = $window.document.getElementsByClassName('watermark'),
					len = els.length;
				while (len--) {
					var img = els[len];					
					img.removeAttribute('data-original-src');					
				}

				$scope.closePostcardModal();
			},
			function(err) {
				console.log(err);
			},
			$window.document.getElementById('watermark-canvas')
		);

	}

	 $scope.closePostcardModal = function() {
	 	$scope.modal.hide();
	 };

	 //Cleanup the modal when we're done with it!
	 $scope.$on('$destroy', function() {
		 $scope.modal.remove();
	 });

	 // Execute action on hide modal
	 $scope.$on('modal.hidden', function() {

	 });

	 // Execute action on remove modal
	 $scope.$on('modal.removed', function() {
	 // Execute action
	 });
});
