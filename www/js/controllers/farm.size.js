angular.module('originsApp.controllers.farm')

.controller('FarmSaveController.size', function($scope, $stateParams, Storage, $http, $window, $state, SERVICE_CONFIG, my_alert) {
    
    $scope.farm = {};
    $scope.farm.size = {};
    $scope.farm.size.unit = 'hectares';
    $scope.farm.size.planted_in_coffee_unit = 'hectares';
    $scope.farm_id = $stateParams.farm_id;
    
       
    Storage.get($scope.farm_id).then(function(response) {
        $scope.farm = response;
        if (response.size != undefined) {
            $scope.farm.size = response.size;
        }
        else {
            $scope.farm.size = {};
            $scope.farm.size.unit = 'hectares';
            $scope.farm.size.planted_in_coffee_unit = 'hectares';   
        }
    });
    
    
    var next = false;
    $scope.sizeSubmit = function(next) {
        $scope.farm_id = $stateParams.farm_id;        
        go_next = false;     
        if (next) next = true;
        Storage.update($scope.farm_id, $scope.farm.size, 'size').then(function() {
            if (next) $state.go('farm-save.varieties');
            else my_alert.show('Data Saved Successfully.');
        });
    }
})
