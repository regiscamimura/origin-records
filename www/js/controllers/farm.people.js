angular.module('originsApp.controllers.farm')

.controller('FarmSaveController.people', function($scope, $stateParams, Storage, $http, $window, $state, SERVICE_CONFIG, my_alert) {
    
    $scope.farm = {};
    $scope.farm.people = {};
    $scope.farm.people.other = [];
    $scope.farm_id = $stateParams.farm_id;
    
    $scope.people_listing = [];
    
    Storage.get($scope.farm_id).then(function(response) {
        $scope.farm = response;
        if (response.people != undefined) {
            $scope.farm.people = response.people;
            if (response.people.other != undefined) {
                for (var i in response.people.other) {
                    if (i == 0) continue;
                    $scope.people_listing.push(response.people.other[i]);
                }
            }
        }
    });
    
    $scope.addPeople = function() {
       $scope.people_listing.push({}); 
    };
    
    $scope.removePeople = function(index) {
        $scope.people_listing.splice(index, 1);
        delete $scope.farm.people.other[index+1];
    };
    
    var next = false;
    $scope.peopleSubmit = function(next) {
        $scope.farm_id = $stateParams.farm_id;        
        go_next = false;     
        if (next) next = true;
        Storage.update($scope.farm_id, $scope.farm.people, 'people').then(function() {
            if (next) $state.go('farm-save.size');
            else my_alert.show('Data Saved Successfully.');
        });
    }
})
