angular.module('originsApp.controllers.farm')

.controller('FarmSaveController.plans', function($scope, $stateParams, Storage, $http, $window, $state, SERVICE_CONFIG, my_alert) {
    
    $scope.farm = {};
    var farm_id = $scope.farm_id = $stateParams.farm_id; 
    
    Storage.get($scope.farm_id).then(function(response) {
        $scope.farm = response;
    });
    
        
    var next = false;
    $scope.plansSubmit = function(next) {
        $scope.farm_id = $stateParams.farm_id;        
        go_next = false;     
        if (next) next = true;
        Storage.update($scope.farm_id, $scope.farm.plans, 'plans').then(function() {
            if (next) $state.go('farm-save.photos');
            else my_alert.show('Data Saved Successfully.');
        });
    }
})
