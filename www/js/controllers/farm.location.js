angular.module('originsApp.controllers.farm')

.controller('FarmSaveController.location', function($scope, $stateParams, Storage, $http, $window, $state, $cordovaGeolocation, SERVICE_CONFIG, my_alert) {
    
    $scope.farm = {};
    $scope.farm.location = {};
    $scope.farm_id = $stateParams.farm_id;
    
    Storage.get($scope.farm_id).then(function(response) {
        $scope.farm = response;
        if (response.location == undefined) {
            $scope.farm.location = {};    
        }
    });
    
    $http.get('js/data/country.json').success(function(data){
        $scope.country_listing = data;
    });
    
        
    $scope.getAlt = function() {      
    
        if (SERVICE_CONFIG == 'dev') {
            $window.navigator.geolocation.getCurrentPosition(function(position) {
                $scope.$apply(function() {
                    $scope.farm.location.alt = Math.round(position.coords.altitude);
                });
            }, function(error) {
                alert(error);   
            });
        }
        else {
            $cordovaGeolocation.getCurrentPosition({timeout: 20000, enableHighAccuracy: true})
            .then(function(position){
                $scope.farm.location.alt = Math.round(position.coords.altitude);
            }, function(error){
                //alert(error);
                console.log(error);
            });
        }
    }
    
    $scope.getLatLng = function() {      
    
        if (SERVICE_CONFIG == 'dev'){
            $window.navigator.geolocation.getCurrentPosition(function(position) {
                $scope.$apply(function() {
                    $scope.farm.location.lat = position.coords.latitude;
                    $scope.farm.location.lng = position.coords.longitude;  
                })
            }, function(error) {
                alert(error);   
            });    
        }
        else {
            $cordovaGeolocation.getCurrentPosition({timeout: 20000, enableHighAccuracy: true})
            .then(function(position){
                var lat  = position.coords.latitude;
                var lng = position.coords.longitude;

                $scope.farm.location.lat = lat;
                $scope.farm.location.lng = lng;
            }, function(error){
                alert(error);
                console.log(error);
            });
        }
    }
    
    var next = false;
    $scope.locationSubmit = function(next) {
        $scope.farm_id = $stateParams.farm_id;        
        go_next = false;     
        if (next) next = true;
        Storage.update($scope.farm_id, $scope.farm.location, 'location').then(function() {
            if (next) $state.go('farm-save.people');
            else my_alert.show('Data Saved Successfully.');
        });
    }
})
