angular.module('originsApp.controllers.farm')

.controller('FarmViewController', function($scope, $stateParams, Storage, $http, $window, $state, SERVICE_CONFIG, $ionicPopup, $ionicPlatform, my_alert) {
    
    $scope.farm = {};
    var farm_id = $scope.farm_id = $stateParams.farm_id; 

    $scope.playing = {};

    $scope.carouselIndex = 0;
    $scope.slides = [];
    
    Storage.get($scope.farm_id).then(function(response) {
        $scope.farm = response;
        if ($scope.farm.photos !== undefined) {
            for (var i in $scope.farm.photos) {
                $scope.slides.push({'id': i, 'img': $scope.farm.photos[i]});
            }
        }
    });



    $scope.delete_record = function(id) {
        $ionicPopup.show({
            title: '<h3>Delete Record</h3>',
            template: 'Are you sure you want to delete this record?',
            cssClass: 'ci-popup',
            scope: $scope,
            buttons: [
                { 
                    text: 'Cancel',
                    type: 'button-default',
                    onTap: function(e) {
                       
                    }
                }, 
                {
                    text: 'OK',
                    type: 'button-positive',
                    onTap: function(e) {
                        Storage.delete_record(id).then(function(response){
                            $state.go('farm-index')
                        });
                    }
                }
            ]
        });
        
    }

    $scope.playAudio = function(index) {

        $ionicPlatform.ready(function() {

            var src = $scope.farm.legacy.audio[index];

            var my_media = new Media(src,
                // success callback
                function() {
                    //console.log("playAudio():Audio Success");
                },
                // error callback
                function(err) {
                   // console.log("playAudio():Audio Error: "+err);
                }
            );

            if ($scope.playing[index] == 1) {
                my_media.pause();
                $scope.playing[index] = 0;
            }
            else {
                my_media.play();
                $scope.playing[index] = 1;
            }
           
        });
    }
    
})
