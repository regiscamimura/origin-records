angular.module('originsApp.controllers.farm')

.controller('FarmSaveController.photos', function($scope, $stateParams, Storage, $http, $window, $state, SERVICE_CONFIG, $ionicPlatform, $cordovaCamera, $timeout, my_alert) {

    $scope.farm = {};
    $scope.farm.photos = {};

    var farm_id = $scope.farm_id = $stateParams.farm_id;


    Storage.get($scope.farm_id).then(function(response) {
        $scope.farm = response;
        if (response.photos == undefined) $scope.farm.photos = {};

        $scope.index = Object.keys($scope.farm.photos).length;
    });

    $scope.capturePhoto = function() {

        var index = $scope.index = Object.keys($scope.farm.photos).length;

        if (window.plugin && window.plugin.statusbarOverlay) {
            window.plugin.statusbarOverlay.hide();
        }

        $ionicPlatform.ready(function() {
            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 1024,
                targetHeight: 768,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageUri) {

                if (window.plugin && window.plugin.statusbarOverlay) {
                    window.plugin.statusbarOverlay.hide();
                }

                $scope.onImageSuccess(imageUri);

            },
            function(err) {
                // error
            });
        });
    }

    $scope.selectPhoto = function() {

        var index = $scope.index = Object.keys($scope.farm.photos).length;

        if (window.plugin && window.plugin.statusbarOverlay) {
            window.plugin.statusbarOverlay.hide();
        }

        $ionicPlatform.ready(function() {
            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 1024,
                targetHeight: 768,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageUri) {

                if (window.plugin && window.plugin.statusbarOverlay) {
                    window.plugin.statusbarOverlay.hide();
                }


                $scope.onImageSuccess(imageUri);
            },
            function(err) {
                // error
            });
        });
    }

    $scope.onImageSuccess = function (fileURI) {
        $scope.createFileEntry(fileURI);
    }

    $scope.createFileEntry = function (fileURI) {
        window.resolveLocalFileSystemURL(fileURI, $scope.copyFile, $scope.fail);
    }

    // 5
    $scope.copyFile = function (fileEntry) {
        var name = fileEntry.fullPath.substr(fileEntry.fullPath.lastIndexOf('/') + 1);
        var newName = $scope.makeid() + name;

        window.resolveLocalFileSystemURL(cordova.file.documentsDirectory, function(fileSystem2) {
            fileEntry.copyTo(
                fileSystem2,
                newName,
                $scope.onCopySuccess,
                $scope.fail
            );
        },
        $scope.fail);
    }

    // 6
    $scope.onCopySuccess = function (entry) {
        $scope.$apply(function () {
            $scope.farm.photos[$scope.index] = entry.nativeURL.replace(cordova.file.documentsDirectory, "");
        });
    }

    $scope.fail = function (error) {
        console.log("fail: " + error.code);
    }

    $scope.makeid = function () {

        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for ( var i=0; i < 5; i++ ) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return $scope.farm_id + '-' + text;
    }



    $scope.removePhoto = function(id) {

        delete $scope.farm.photos[id];
    };

    var next = false;
    $scope.photosSubmit = function(next) {
        $scope.farm_id = $stateParams.farm_id;
        go_next = false;
        if (next) next = true;
        Storage.update($scope.farm_id, $scope.farm.photos, 'photos').then(function() {
            if (next) $state.go('farm-save.main');
            else my_alert.show('Data Saved Successfully.');
        });
    }
})
