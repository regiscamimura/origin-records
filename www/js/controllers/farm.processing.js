angular.module('originsApp.controllers.farm')

.controller('FarmSaveController.processing', function($scope, $stateParams, $ionicModal, $ionicPlatform, Storage, VideoStorage, $http, $window, $state, SERVICE_CONFIG, $cordovaCapture, $cordovaCamera, $timeout, my_alert) {
    
    $scope.farm = {};
    var farm_id = $scope.farm_id = $stateParams.farm_id;    

    $http.get('js/data/processing-method.json').success(function(data){
        $scope.processing_method_listing = data;
    });

    $http.get('js/data/drying-method.json').success(function(data){
        $scope.drying_method_listing = data;
    });
    
    Storage.get($scope.farm_id).then(function(response) {
        $scope.farm = response;
        if ($scope.farm.processing == undefined) $scope.farm.processing = {};
    });
    
    
    var next = false;
    $scope.processingSubmit = function(next) {
        $scope.farm_id = $stateParams.farm_id;        
        go_next = false;     
        if (next) next = true;
        Storage.update($scope.farm_id, $scope.farm.processing, 'processing').then(function() {
            if (next) $state.go('farm-save.legacy');
            else my_alert.show('Data Saved Successfully.');
        });
    }


    $scope.urlForClipThumb = function(clipUrl) {
        var name = clipUrl.substr(clipUrl.lastIndexOf('/') + 1);
        var trueOrigin = cordova.file.dataDirectory + name;
        var sliced = clipUrl.slice(0, -4);
        return sliced + '.png';
    }
     
    $scope.showClip = function(clip) {
        $scope.showModal('views/video-modal.html');   
    }

    $scope.clip = '';
    $scope.captureVideo = function() {
        var options = { duration: 600 };

        $cordovaCapture.captureVideo(options).then(function(videoData) {
            VideoStorage.saveVideo(videoData, farm_id).success(function(data) {
                $scope.clip = data;

                $timeout(function() { 
                    $scope.farm.processing.video = $scope.clip;
                    $scope.farm.processing.video_thumb = $scope.urlForClipThumb($scope.clip);
    
                    $scope.$apply();
                }, 0, false);
            }).error(function(data) {
                console.log('ERROR: ' + data);
            });

            
        }, function(err) {
            // An error occurred. Show a message to the user
        });
    }


    $scope.selectVideo = function() {

        $ionicPlatform.ready(function() {
            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                mediaType: Camera.MediaType.VIDEO,
                encodingType: Camera.EncodingType.PNG,
                targetWidth: 1024,
                targetHeight: 768,
                popoverOptions: CameraPopoverOptions
            };

            $cordovaCamera.getPicture(options).then(function(data) {

                if (window.plugin && window.plugin.statusbarOverlay) {
                    window.plugin.statusbarOverlay.hide();
                }

                $scope.clip = data;

                $timeout(function() { 
                    $scope.farm.processing.video = $scope.clip;

                    var name = $scope.clip.slice(0, -4);
                    window.PKVideoThumbnail.createThumbnail ($scope.clip, name + '.png', function(prevSucc) {
                        $scope.farm.processing.video_thumb = $scope.urlForClipThumb($scope.clip);
                        $scope.$apply();
                    });
    
                    
                    if (window.plugin && window.plugin.statusbarOverlay) {
                        window.plugin.statusbarOverlay.hide();
                    }
                }, 0, false);
            }, 
            function(err) {
                // error
            });
        });
    }

    $scope.showModal = function(templateUrl) {
        $ionicModal.fromTemplateUrl(templateUrl, {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });
    }

        // Close the modal
    $scope.closeModal = function() {
        $scope.modal.hide();
        $scope.modal.remove();
    };
})
