angular.module('originsApp.controllers.settings', [])

.controller('SettingsController', function ($scope, Auth, SERVICE_CONFIG, $state, $rootScope, $http) {
    
    $scope.rec = {};
    
    $scope.status = "";
    $scope.message = "";

    $scope.settings = {'first_name': Auth.first_name(), 'last_name': Auth.last_name(), 'username': Auth.username(), 'password': '', 'confirm_password': '', 'multi_user': Auth.multi_user(), 'wifi_sync_only': Auth.wifi_sync_only(), 'id':Auth.user_id()};


    if (!Auth.user_id()) $state.go("index");

    $scope.logout = function() {
        Auth.logout();
    }
    
    $scope.submit = function() {
        
        $scope.status = "";
        $scope.message = "";

        if ($scope.settings['wifi_sync_only']) $scope.settings['wifi_sync_only'] = 1;
        else $scope.settings['wifi_sync_only'] = 0;
        if ($scope.settings['multi_user']) $scope.settings['multi_user'] = 1;
        else $scope.settings['multi_user'] = 0;

        if (!$scope.settings['first_name']) {
            $scope.status = 'error';
            $scope.message += "Please insert a first name; ";
        }
        if (!$rootScope.validateEmail($scope.settings['username'])) {
            $scope.status = 'error';
            $scope.message += "Please insert a valid email name; ";
        }
        if ($scope.settings['password'] && $scope.settings['password'] != $scope.settings['confirm_password']) {
            $scope.status = 'error';
            $scope.message += "Please confirm the password; ";
        }


        $rootScope.$broadcast('loading:show');
        
        Auth.first_name($scope.settings.first_name);
        Auth.last_name($scope.settings.last_name);
        Auth.username($scope.settings.username);
        
        Auth.wifi_sync_only($scope.settings.wifi_sync_only);
        Auth.multi_user($scope.settings.multi_user);
        
        $http
            .post(SERVICE_CONFIG.base_url+'user', $scope.settings)
            .success(function (response) {
            if (response.status == 'success') {
                $scope.status = 'success';
                $scope.message = 'Settings saved successfully.';
                $scope.settings.wifi_sync_only = Auth.wifi_sync_only();
                $scope.settings.multi_user = Auth.multi_user();
                $rootScope.$broadcast('loading:hide');
            }
        })
        .error(function(response) {

            $scope.status = 'error';
            $scope.message = 'Settings were saved locally only, unable to connect to web.';
            $scope.settings.wifi_sync_only = Auth.wifi_sync_only();
            $scope.settings.multi_user = Auth.multi_user();
            $rootScope.$broadcast('loading:hide');
        });
    }   

})
