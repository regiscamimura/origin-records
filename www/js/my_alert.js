angular.module('originsApp.my_alert', []) 
.service('my_alert', function($ionicPopup) {

    var self = this;
    
    this.show = function(message) {
        $ionicPopup.show({
            title: '<h3>Origin Record</h3>',
            template: message,
            cssClass: 'ci-popup',
            buttons: [
                {
                    text: 'OK',
                    type: 'button-positive',
                    onTap: function(e) {
                        
                    }
                }
            ]
        });
    }
    
})