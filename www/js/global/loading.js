app.run(function($rootScope, $ionicLoading) {
	$rootScope.$on('loading:show', function() {
		$ionicLoading.show({template: 'Please wait...'});
	});

	$rootScope.$on('loading:hide', function() {
		$ionicLoading.hide();
	});
});