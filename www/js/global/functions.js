app.run(function($rootScope, $ionicPlatform) {

	$ionicPlatform.ready(function() {

		$rootScope.full_path = function(filename) {

			if (filename == undefined) return;

			filename = filename.substring(filename.lastIndexOf('/') + 1);

			return cordova.file.documentsDirectory.replace("file://", "") + filename;

		}

		$rootScope.sizeOf = function(obj) {
		    return Object.keys(obj).length;
		};

		$rootScope.hectares = function(value, unit) {
			if (unit == 'hectares') return value;
			else {
				return value = value * 0.404686;
			}
		}

		$rootScope.gotFile = function(fileEntry) {

			fileEntry.file(function(file) {
				$rootScope.upload_size += file.size;
			});
		}

		$rootScope.fileFail = function(ent) {
			console.log(ent);
		}

		$rootScope.validateEmail = function(email) {
		    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		    return re.test(email);
		}

		$rootScope.makeid = function(farm_id) {
			var text = '';
			var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
			for (var i = 0; i < 5; i++) {
				text += possible.charAt(Math.floor(Math.random() * possible.length));
			}

			if (farm_id) text = farm_id + '-' + text;

			return text;

		}
	});
});
