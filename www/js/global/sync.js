app.run(function($rootScope, $ionicPopup, $ionicPlatform, Auth) {
	

	$rootScope.sync = function() {

        $ionicPlatform.ready(function() {
            if(window.Connection) {
                if(navigator.connection.type == Connection.NONE) {
                    $ionicPopup.show({
                        title: '<h3>Syncronize with web account</h3>',
                        template: 'Device not connected to the internet.',
                        cssClass: 'ci-popup',
                        buttons: [
                            { 
                                text: 'Close',
                                type: 'button-default',
                                onTap: function(e) {
                              
                                }
                            }
                        ]
                    });
                }
                else if(Auth.wifi_sync_only() && navigator.connection.type == Connection.WIFI) {
                    $rootScope.sync_popup();
                }
                else {
                    $ionicPopup.show({
                        title: '<h3>Syncronize with web account</h3>',
                        template: 'You can only sync your records when connected to the internet over an wi-fi network.',
                        cssClass: 'ci-popup',
                        buttons: [
                            { 
                                text: 'Close',
                                type: 'button-default',
                                onTap: function(e) {
                              
                                }
                            }
                        ]
                    });
                }
        		
            }
        });

	};

    $rootScope.sync_popup = function() {
        $ionicPopup.show({
            title: '<h3>Syncronize with web account</h3>',
            templateUrl: 'views/sync.html',
            cssClass: 'ci-popup',
            buttons: [
                { 
                    text: 'Close',
                    type: 'button-default',
                    onTap: function(e) {
                  
                    }
                }, 
                { 
                    text: 'Download',
                    type: 'button-balanced',
                    onTap: function(e) {
                        e.preventDefault();
                        return $rootScope.sync_download();
                    }
                }, 
                {
                    text: 'Upload',
                    type: 'button-positive',
                    onTap: function(e) {
                        e.preventDefault();
                        return $rootScope.sync_upload();
                    }
                }
            ]
        });
    }
});