app.run(function($rootScope, $ionicPlatform, $http, Storage, SERVICE_CONFIG, $cordovaFileTransfer, $timeout, $q, Auth, $state) {

	$rootScope.download_processing = false;
	$rootScope.downloadProgress = {};

	$rootScope.sync_download = function(action) {

		if ($rootScope.download_processing == true) return;

		$ionicPlatform.ready(function() {

			if (!action) {

				$rootScope.download_listing = {};
				$rootScope.download_size = 0;
				$rootScope.download_data = {};
				$rootScope.download_progress_percentage = 0;
				$rootScope.download_progress_size = 0;

				$http
		        .post(
		        	SERVICE_CONFIG.base_url+'download/index',
		        	{'user_id': Auth.user_id(), 'multi_user': Auth.multi_user()}
		        )
		        .success(function(response, status, headers, config) {

		        	console.log(response);

				    $rootScope.download_size = response['total'];
				    $rootScope.download_listing = response['main']['data'];
				    $rootScope.download_data = response;
				    $rootScope.download_processing = true;

				    $rootScope.downloadRecords(); 

				}).
				error(function(data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
				});

		    }
		});
	}

	$rootScope.downloadRecords = function() {

		if ($rootScope.sizeOf($rootScope.download_listing) < 1) {
			$rootScope.$broadcast("download-completed");
			return;
		}

		for (var i in $rootScope.download_listing) {
			var record = $rootScope.download_listing[i];
			Storage.get(record['id']).then(

				function(rec) {

				
					$rootScope.download_progress_size += record['data_size'];
					$rootScope.download_progress_percentage = (($rootScope.download_progress_size) / $rootScope.download_size) * 100;
					delete $rootScope.download_listing[i];
					$rootScope.downloadRecords();
					return;
				},
				function() {

					Storage.import(record).then(function() {
						$rootScope.download_progress_size += record['data_size'];
						$rootScope.download_progress_percentage = (($rootScope.download_progress_size) / $rootScope.download_size) * 100;
						
						if ($rootScope.download_data['media'][record['id']] == undefined) {
							delete $rootScope.download_listing[i];
							$rootScope.downloadRecords();
							
						}
						else {
							var promises = [];
							var promise;


							for (var j in $rootScope.download_data['media'][record['id']]) {

								var media = $rootScope.download_data['media'][record['id']][j];
								
								promise = $rootScope.downloadFile(media);
								promises.push(promise);
							}

							$q.all(promises).then(function() {
							    delete $rootScope.download_listing[i];
				                $rootScope.downloadRecords();
				                
							});
						}
					});
				}
			);

			break;
		}
	}

	
	
	$rootScope.downloadFile = function(media) {

		var deferred = $q.defer();

		if (media.type != 'audio') var targetPath = cordova.file.dataDirectory + media.filename;
		else var targetPath = cordova.file.documentsDirectory + media.filename;

		var trustHosts = true;
    	var options = {};

    	var temp_id = Storage.makeid();

		$cordovaFileTransfer.download(media.url, targetPath, options, trustHosts)
		.then(
			function(result) {
				deferred.resolve({});
			}, 
			function(err) {
				deferred.resolve({});
			}, 
			function (progress) {
				$timeout(function () {
					$rootScope.downloadProgress[temp_id] = progress.loaded;

					if (progress.loaded == progress.total) {
						$rootScope.download_progress_size += progress.total;
						delete $rootScope.downloadProgress[temp_id];
					}
				    else {
				    	var loaded = 0;
				    	for (var i in $rootScope.downloadProgress) {
				    		loaded += $rootScope.downloadProgress[i];
				    	}
				    	$rootScope.download_progress_percentage = (($rootScope.download_progress_size + loaded) / $rootScope.download_size) * 100;
				    }
				})
			}
		);

		
		return deferred.promise;
	}


	

	$rootScope.$on("download-completed", function() {
		$rootScope.download_progress_percentage = 100;

		$timeout(function() {
			$rootScope.download_listing = {};
			$rootScope.download_size = 0;
			$rootScope.download_progress_size = 0;
			$rootScope.download_processing = false;
			$rootScope.download_progress_percentage = 0;
			$state.go($state.current, {}, {reload: true});
		}, 1500);
	});
});