app.run(function($rootScope, $ionicPopup, $ionicPlatform, $http, Storage, SERVICE_CONFIG, $cordovaFileTransfer, $cordovaDevice, $timeout, $q, Auth, $state, my_alert, $window) {

	
	$rootScope.email = function(id) {	

		$rootScope.email_error = "";

		$ionicPlatform.ready(function() {			

			$rootScope.email_popup = $ionicPopup.show({
	            title: '<h3>Email record</h3>',
	            templateUrl: 'views/email.html',
	            cssClass: 'ci-popup',
	            buttons: [
	            	{ 
	                    text: 'Close',
	                    type: 'button-default',
	                    onTap: function(e) {
	                  
	                    }
	                }, 
	                { 
	                    text: 'Send',
	                    type: 'button-positive',
	                    onTap: function(e) {
	                    	e.preventDefault();
	                    	var email_to = $window.document.getElementById('email-to');
	                    	var email_from = $window.document.getElementById('email-from');

	                    	$rootScope.email_error = "";
	                    	if (!$rootScope.validateEmail(email_to.value)) $rootScope.email_error = "Please insert a valid email address.";
	                        else return $rootScope.email_send(id, email_to.value, email_from.value);
	                    }
	                }
	            ]
	        });
		});
	}

	$rootScope.email_progress_percentage = 0;
	$rootScope.email_processing = false;
	$rootScope.emailProgress = {};
	

	$rootScope.email_send = function(id, email_to, email_from) {
		//console.log(id);
		//console.log(email_to);

		if ($rootScope.email_processing == true) {
			$rootScope.email_please_wait = true;
			return;
		}


		$ionicPlatform.ready(function() {

			Storage.get(id).then(
				function(record) {
					if (record.sync == 1) {
						$rootScope.email_processing = true;
						$rootScope.email_request(record, email_to, email_from);
						return;
					}

					$rootScope.email_size = 0;
					$rootScope.email_progress_percentage = 0;
					$rootScope.email_progress_size = 0;


					$rootScope.calculate_email_size(record);
					$rootScope.email_upload(record, email_to, email_from);
					$rootScope.email_processing = true;

				},
				function () {
					alert('Record Not Found');
					$rootScope.$broadcast('email-completed');
				}
			)
		});
	}

	$rootScope.email_upload = function(record, email_to, email_from) {

		var promises = [];
		var promise;

		if (record['processing'] != undefined && record['processing']['video'] != undefined) {
			promise = $rootScope.emailUploadFile(record['processing']['video'], 'video/quicktime', record);
			promises.push(promise);

		}
		if (record['processing'] != undefined && record['processing']['video_thumb'] != undefined) {
			promise = $rootScope.emailUploadFile(record['processing']['video_thumb'], 'image/png', record);
			promises.push(promise);
		}
		if (record['legacy'] != undefined && record['legacy']['audio'] != undefined) {
			for (var j in record['legacy']['audio']) {
				var name = record['legacy']['audio'][j].split('//')[1];
				promise = $rootScope.emailUploadFile(cordova.file.documentsDirectory + name, 'audio/wav', record);
				promises.push(promise);
			}
		}
		if (record['photos'] != undefined) {
			for (var j in record['photos']) {
				promise = $rootScope.emailUploadFile(record['photos'][j], 'image/png', record);
				promises.push(promise);
			}
		}

		$q.all(promises).then(function() {
			Storage.update(record['id'], {'sync': '1'});
		    $rootScope.email_request(record, email_to, email_from);            
		}, function() {
			$rootScope.$broadcast('email-completed');
		});
	}

	$rootScope.emailUploadFile = function(fileName, mimetype, record) {

		var deferred = $q.defer();

		var filename = fileName.substring(fileName.lastIndexOf('/')+1);

		var user_id = Auth.user_id();
		if (!user_id) user_id = 'no-account-' + $cordovaDevice.getUUID();
		else if (record) user_id = record['created_by'];

		var params = { 'user_id': user_id }
		if (record) params['id'] = record['id'];

		var options = {
		    fileKey: "upload",
		    fileName: filename,
		    chunkedMode: true,
		    mimeType: mimetype,
		    params: params
		};

		var server = SERVICE_CONFIG.base_url+'upload';

		var temp_id = Storage.makeid();
		$rootScope.emailProgress[temp_id] = 0;


		$cordovaFileTransfer.upload(server, fileName, options)
		.then(function(result) {
			//console.log(result);
			deferred.resolve({});

		}, 
		function(err) {
			//console.log(err);
			deferred.resolve({});
		}, 
		function (progress) {
			$timeout(function () {

				$rootScope.emailProgress[temp_id] = progress.loaded;

				if (progress.loaded == progress.total) {
					$rootScope.email_progress_size += progress.total;
					delete $rootScope.emailProgress[temp_id];
				}
			    else {
			    	var loaded = 0;
			    	for (var i in $rootScope.emailProgress) {
			    		loaded += $rootScope.emailProgress[i];
			    	}
			    	$rootScope.email_progress_percentage = (($rootScope.email_progress_size + loaded) / $rootScope.email_size) * 100;
			    }
			});
		});

		return deferred.promise;
	}

	$rootScope.email_request = function(record, email_to, email_from) {

		var user_id;
		if (!Auth.user_id()) user_id = 'no-account-' + $cordovaDevice.getUUID();
		else user_id = record['created_by'];

		var row = angular.toJson(record);

		$http
        .post(
        	SERVICE_CONFIG.base_url+'email/index',
        	{
        		'record': row,
        		'email_to': email_to,
        		'email_from': email_from,
        		'user_id': user_id
        	}
        )
        .success(function(response) {
        	$rootScope.$broadcast('email-completed');
        })
        .error(function(response) {
        	$rootScope.$broadcast('email-completed');
        });
	}

	$rootScope.calculate_email_size = function(record) {

		$rootScope.email_size = 0;	
		

		if (record['processing'] != undefined && record['processing']['video'] != undefined) {
			window.resolveLocalFileSystemURL(record['processing']['video'], $rootScope.emailGotFile, $rootScope.fileFail);
		}
		if (record['processing'] != undefined && record['processing']['video_thumb'] != undefined) {
			window.resolveLocalFileSystemURL(record['processing']['video_thumb'], $rootScope.emailGotFile, $rootScope.fileFail);
		}
		if (record['legacy'] != undefined && record['legacy']['audio'] != undefined) {
			for (var j in record['legacy']['audio']) {
				var name = record['legacy']['audio'][j].split('//')[1];
				window.resolveLocalFileSystemURL(cordova.file.documentsDirectory + name, $rootScope.emailGotFile, $rootScope.fileFail);
			}
		}
		if (record['photos'] != undefined) {
			for (var j in record['photos']) {
				window.resolveLocalFileSystemURL(record['photos'][j], $rootScope.emailGotFile, $rootScope.fileFail);
			}
		}
		
	}

	$rootScope.emailGotFile = function(fileEntry) {		

		fileEntry.file(function(file) {
			$rootScope.email_size += file.size;
		});
	}

	$rootScope.$on("email-completed", function() {
		$rootScope.email_progress_percentage = 100;

		$timeout(function() {
			
			$rootScope.email_size = 0;
			$rootScope.email_progress_size = 0;
			$rootScope.email_processing = false;
			$rootScope.email_progress_percentage = 0;
			$rootScope.email_please_wait = false;
			$rootScope.email_popup.close();
		}, 1500);
	});


});