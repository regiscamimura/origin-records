app.run(function($rootScope, $ionicPlatform, $http, Storage, SERVICE_CONFIG, $cordovaFileTransfer, $timeout, $q, Auth) {

	$rootScope.upload_processing = false;
	$rootScope.uploadProgress = {};

	$rootScope.sync_upload = function(action) {

		if ($rootScope.upload_processing == true) return;

		$ionicPlatform.ready(function() {

			if (!action) {

				$rootScope.sync_listing = {};
				$rootScope.deleted_listing = {};
				$rootScope.upload_size = 0;
				$rootScope.upload_progress_percentage = 0;
				$rootScope.upload_progress_size = 0;


				Storage.listing('deleted').then(function(deleted_data){
					$rootScope.deleted_listing = deleted_data;
					Storage.listing().then(function(response) {
						$rootScope.sync_listing = response;
						for (var i in response) {
							if (response[i]['sync'] == '1') {
								delete $rootScope.sync_listing[i];
							}
						}
						$rootScope.calculate_upload();
						$rootScope.sync_upload('go');
						$rootScope.upload_processing = true;
			     	});
				})

		    }
		    else {


		    	$rootScope.uploadFile(Storage.file_path('deleted'), 'text/plain').then(function() {
		    		$rootScope.uploadProgress = {};
		    		$http.delete(
			        	SERVICE_CONFIG.base_url+'upload/index?user_id='+Auth.user_id()
			        ).success(function(response) {
			        	//console.log(response);
			        }).error(function(response) {
		    			//console.log(response);
		    		});

			    	$rootScope.uploadFile(Storage.file_path(), 'text/plain').then(function(){
			    		$rootScope.uploadMedia();
					});
				});
			}
		});
	}

	$rootScope.uploadMedia = function() {

		var promises = [];
		var promise;

		if ($rootScope.sizeOf($rootScope.sync_listing) < 1 ) {
			$rootScope.$broadcast("upload-completed");
			return;
		}

		for (var i in $rootScope.sync_listing) {


			if ($rootScope.sync_listing[i]['sync'] == '1') {
				delete $rootScope.sync_listing[i];
				$rootScope.uploadMedia();
				continue;
			}

			var record = $rootScope.sync_listing[i];
			var row = angular.toJson(record);

			$http
	        .post(
	        	SERVICE_CONFIG.base_url+'farm/index',
	        	[row]
	        )
	      	.success(function(response) {

	      		if (record['processing'] != undefined && record['processing']['video'] != undefined) {
					promise = $rootScope.uploadFile(path(record['processing']['video']), 'video/quicktime', record);
					promises.push(promise);

				}
				if (record['processing'] != undefined && record['processing']['video_thumb'] != undefined) {
					promise = $rootScope.uploadFile(path(record['processing']['video_thumb']), 'image/png', record);
					promises.push(promise);
				}
				if (record['legacy'] != undefined && record['legacy']['audio'] != undefined) {
					for (var j in record['legacy']['audio']) {
						var name = record['legacy']['audio'][j].split('//')[1];
						promise = $rootScope.uploadFile(cordova.file.documentsDirectory + name, 'audio/wav', record);
						promises.push(promise);
					}
				}
				if (record['photos'] != undefined) {
					for (var j in record['photos']) {
						promise = $rootScope.uploadFile(path(record['photos'][j]), 'image/png', record);
						promises.push(promise);
					}
				}

				$q.all(promises).then(function() {
				    Storage.update($rootScope.sync_listing[i]['id'], {'sync': '1'});
	                delete $rootScope.sync_listing[i];
	                $rootScope.uploadMedia();

				});
			}).error(function(response) {
				console.log(response);
			});

			break;
		}
	}


	$rootScope.uploadFile = function(fileName, mimetype, record) {

		var deferred = $q.defer();

		var filename = fileName.substring(fileName.lastIndexOf('/')+1);

		if (record) {
			var params = { 'user_id': record['created_by'] }
			params['id'] = record['id'];
		}
		else var params = { 'user_id': Auth.user_id() }

		var options = {
		    fileKey: "upload",
		    fileName: filename,
		    chunkedMode: true,
		    mimeType: mimetype,
		    params: params
		};

		var server = SERVICE_CONFIG.base_url+'upload';

		var temp_id = Storage.makeid();
		$rootScope.uploadProgress[temp_id] = 0;


		$cordovaFileTransfer.upload(server, fileName, options)
		.then(function(result) {
			deferred.resolve({});

		},
		function(err) {
			deferred.resolve({});
		},
		function (progress) {
			$timeout(function () {

				$rootScope.uploadProgress[temp_id] = progress.loaded;

				if (progress.loaded == progress.total) {
					$rootScope.upload_progress_size += progress.total;
					delete $rootScope.uploadProgress[temp_id];
				}
			    else {
			    	var loaded = 0;
			    	for (var i in $rootScope.uploadProgress) {
			    		loaded += $rootScope.uploadProgress[i];
			    	}
			    	$rootScope.upload_progress_percentage = (($rootScope.upload_progress_size + loaded) / $rootScope.upload_size) * 100;
			    }
			});
		});

		return deferred.promise;
	}


	$rootScope.calculate_upload = function() {

		$rootScope.upload_size = 0;

		window.resolveLocalFileSystemURL(Storage.file_path(), $rootScope.gotFile, $rootScope.fileFail);
		window.resolveLocalFileSystemURL(Storage.file_path('deleted'), $rootScope.gotFile, $rootScope.fileFail);

		for (var i in $rootScope.sync_listing) {
			if ($rootScope.sync_listing[i]['sync'] != '1') {

				var record = $rootScope.sync_listing[i];

				if (record['processing'] != undefined && record['processing']['video'] != undefined) {
					window.resolveLocalFileSystemURL(path(record['processing']['video']), $rootScope.gotFile, $rootScope.fileFail);
				}
				if (record['processing'] != undefined && record['processing']['video_thumb'] != undefined) {
					window.resolveLocalFileSystemURL(path(record['processing']['video_thumb']), $rootScope.gotFile, $rootScope.fileFail);
				}
				if (record['legacy'] != undefined && record['legacy']['audio'] != undefined) {
					for (var j in record['legacy']['audio']) {
						var name = record['legacy']['audio'][j].split('//')[1];
						window.resolveLocalFileSystemURL(cordova.file.documentsDirectory + name, $rootScope.gotFile, $rootScope.fileFail);
					}
				}
				if (record['photos'] != undefined) {
					for (var j in record['photos']) {
						window.resolveLocalFileSystemURL(path(record['photos'][j]), $rootScope.gotFile, $rootScope.fileFail);
					}
				}

			}
		}

	}


	$rootScope.$on("upload-completed", function() {
		$rootScope.upload_progress_percentage = 100;

		$timeout(function() {
			$rootScope.sync_listing = {};
			$rootScope.deleted_listing = {};
			$rootScope.upload_size = 0;
			$rootScope.upload_progress_size = 0;
			$rootScope.upload_processing = false;
			$rootScope.upload_progress_percentage = 0;
		}, 1500);
	});
});
