angular.module('originsApp.storage', ['ngCordova'])
.service('Storage', function($cordovaFile, $window, Auth, SERVICE_CONFIG, $q, $timeout, $ionicPlatform) {

    var self = this;

    this.file = function(type) {
        if (!Auth.user_id()) var file = "no_account";
        else var file = Auth.user_id();

        if (!type) type = 'data';

        return file+"-"+type+".json";
    }


    this.create = function() {

        /*if (SERVICE_CONFIG.mode == 'dev') {
            if (!$window.localStorage[self.file('data')]) {
                $window.localStorage[self.file('data')] = '{}';
            }

            if (!$window.localStorage[self.file('settings')]) {
                $window.localStorage[self.file('setting')] = '{}';
            }
            return;
        }

        $cordovaFile.createFile(cordova.file.dataDirectory, self.file('data'), false)
        .then(function (success) {
            alert('file created');
        }, function (error) {

        });

        $cordovaFile.createFile(cordova.file.dataDirectory, self.file('setting'), false)
        .then(function (success) {
            alert('file created');
        }, function (error) {

        }); */
    }

    this.makeid = function() {

        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });

        var prefix = Auth.user_id() ? Auth.user_id() : 'no-account';
        return prefix + '-' + uuid;

    }

    this.add = function(data, type) {

        var deferred = $q.defer();

        self.listing(type).then(function(response) {
            var save = response;
            var id = self.makeid();
            save[id] = data;
            save[id]['created_at'] = new Date();
            save[id]['created_by'] = Auth.user_id() ? Auth.user_id() : 'no-account';
            save[id]['id'] = id;
            save[id]['sync'] = '0';

            if (SERVICE_CONFIG.mode == 'dev') {
                $window.localStorage[self.file(type)] = angular.toJson(save);
                $timeout(function(){
                    deferred.resolve({'id': id});
                }, 20);

            }
            else {
                $ionicPlatform.ready(function() {
                    $cordovaFile.writeFile(cordova.file.dataDirectory, self.file(type), angular.toJson(save), true)
                    .then(function (success) {
                        deferred.resolve({'id': id});
                    }, function (error) {
                        deferred.reject('Data not saved.');
                    });
                });
            }

        });

         return deferred.promise;
    }

    this.import = function(data) {

        var deferred = $q.defer();

        self.listing().then(function(response) {
            var save = response;
            var id = data['id'];

            save[id] = data;
            save[id]['sync'] = '1';

            if (data['processing'] != undefined && data['processing']['video'] != undefined) {
                var filename = data['processing']['video'];
                filename = filename.substring(filename.lastIndexOf('/')+1);
                data['processing']['video'] = filename;
            }

            if (data['processing'] != undefined && data['processing']['video_thumb'] != undefined) {
                var filename = data['processing']['video_thumb'];
                filename = filename.substring(filename.lastIndexOf('/')+1);
                data['processing']['video_thumb'] = filename;
            }

            /*if (data['legacy'] != undefined && data['legacy']['audio'] != undefined) {
                for (var i in data['legacy']['audio']) {
                    var filename = data['photos'][i];
                    filename = filename.substring(filename.lastIndexOf('/')+1);
                    data['photos'][i] = cordova.file.dataDirectory + filename;
                }
            }*/

            if (data['photos'] != undefined) {
                for (var i in data['photos']) {
                    var filename = data['photos'][i];
                    filename = filename.substring(filename.lastIndexOf('/')+1);
                    data['photos'][i] = filename;
                }
            }

            if (SERVICE_CONFIG.mode == 'dev') {
                $window.localStorage[self.file()] = angular.toJson(save);
                $timeout(function(){
                    deferred.resolve({'id': id});
                }, 20);

            }
            else {
                $ionicPlatform.ready(function() {
                    $cordovaFile.writeFile(cordova.file.dataDirectory, self.file(), angular.toJson(save), true)
                    .then(function (success) {
                        deferred.resolve({'id': id});
                    }, function (error) {
                        deferred.reject('Data not saved.');
                    });
                });
            }

        });

        return deferred.promise;
    }

    this.delete_record = function(id) {

        var deferred = $q.defer();

        self.listing().then(function(response) {
            var save = response;
            delete save[id];

            if (SERVICE_CONFIG.mode == 'dev') {
                $window.localStorage[self.file()] = angular.toJson(save);
                $timeout(function(){
                   // deferred.resolve({'id': id});
                }, 20);

            }
            else {
                $ionicPlatform.ready(function() {
                    $cordovaFile.writeFile(cordova.file.dataDirectory, self.file(), angular.toJson(save), true)
                    .then(function (success) {
                       // deferred.resolve({'id': id});
                    }, function (error) {
                        deferred.reject('Data not deleted.');
                        return;
                    });
                });
            }

        });

        self.listing('deleted').then(function(response) {
            var save = response;
            save[id] = {};
            save[id]['deleted_at'] = new Date();
            save[id]['deleted_by'] = Auth.user_id() ? Auth.user_id() : 'no-account';
            save[id]['id'] = id;
            save[id]['sync'] = '0';

            if (SERVICE_CONFIG.mode == 'dev') {
                $window.localStorage[self.file('deleted')] = angular.toJson(save);
                $timeout(function(){
                    deferred.resolve({'id': id});
                }, 20);

            }
            else {
                $ionicPlatform.ready(function() {
                    $cordovaFile.writeFile(cordova.file.dataDirectory, self.file('deleted'), angular.toJson(save), true)
                    .then(function (success) {
                        deferred.resolve({'id': id});
                    }, function (error) {
                        deferred.reject('Data not deleted.');
                    });
                });
            }

        });

        return deferred.promise;
    }

    this.update = function(id, data, section) {
        var deferred = $q.defer();

        self.listing().then(function(response) {
            var save = response;
            if (save[id] == undefined) {
                deferred.reject('Record not found.');
            }
            else {

                save[id]['sync'] = '0';

                if (section) {
                    save[id][section] = {};
                    for (var i in data) {
                        save[id][section][i] = data[i];
                    }
                }
                else {
                    for (var i in data) {
                        save[id][i] = data[i];
                    }
                }

                save[id]['updated_at'] = new Date();
                save[id]['updated_by'] = Auth.user_id() ? Auth.user_id() : 'no-account';


                if (SERVICE_CONFIG.mode == 'dev') {
                    $window.localStorage[self.file()] = angular.toJson(save);
                    $timeout(function(){
                        deferred.resolve({'id': id});
                    }, 20);

                }
                else {

                    $ionicPlatform.ready(function() {

                        $cordovaFile.writeFile(cordova.file.dataDirectory, self.file(), angular.toJson(save), true)
                        .then(function (success) {
                            deferred.resolve({'id': id});
                        }, function (error) {
                            deferred.reject('Data not saved.');
                        });
                    });
                }
            }
        });

        return deferred.promise;


    }

    this.length = function(listing) {
         var count = 0;
         for (var i in listing) count++;
         return count;
    }

    this.listing = function(type) {

        var deferred = $q.defer();

        if (SERVICE_CONFIG.mode == 'dev') {
            $timeout(function(){
                if (!$window.localStorage[self.file(type)]) var data = {};
                else var data = angular.fromJson($window.localStorage[self.file(type)]);

                deferred.resolve(data);
            }, 20);

            return deferred.promise;
        }

        $ionicPlatform.ready(function() {
            $cordovaFile.readAsText(cordova.file.dataDirectory, self.file(type))
            .then(function(success) {
                deferred.resolve(angular.fromJson(success));
            }, function (error) {
                var data = {};
                deferred.resolve(data);
            });
        });

        return deferred.promise;
    }

    this.get = function(id) {
        var deferred = $q.defer();

        self.listing().then(function(response) {
            if (response[id] == undefined) {
                deferred.reject('Record not found.');
            }
            else deferred.resolve(response[id]);
        });

        return deferred.promise;
    }

    this.file_path = function(type) {
        if (SERVICE_CONFIG.mode == 'dev') {
            return self.file(type);
        }
        else {
            return cordova.file.documentsDirectory + self.file(type);
        }
    }
})

.service("VideoStorage",  function($q) {

    var deferred = $q.defer();
    var promise = deferred.promise;

    // Resolve the URL to the local file
    // Start the copy process
    function createFileEntry(fileURI, farm_id) {

        deferred = $q.defer();
        promise = deferred.promise;

        promise.success = function(fn) {
            promise.then(fn);
            return promise;
        }
        promise.error = function(fn) {
            promise.then(null, fn);
            return promise;
        }

        window.resolveLocalFileSystemURL(fileURI, function(entry) {
            return copyFile(entry, farm_id);
        }, fail);
    }

    // Create a unique name for the videofile
    // Copy the recorded video to the app dir
    function copyFile(fileEntry, farm_id) {
        var name = fileEntry.fullPath.substr(fileEntry.fullPath.lastIndexOf('/') + 1);
        var newName = makeid(farm_id) + name;

        window.resolveLocalFileSystemURL(cordova.file.documentsDirectory, function(fileSystem2) {
                fileEntry.copyTo(fileSystem2, newName, function(succ) {
                    return onCopySuccess(succ);
                }, fail);
            },
            fail
        );
    }

    // Called on successful copy process
    // Creates a thumbnail from the movie
    // The name is the moviename but with .png instead of .mov
    function onCopySuccess(entry) {
        var name = entry.nativeURL.slice(0, -4);
        window.PKVideoThumbnail.createThumbnail (entry.nativeURL, name + '.png', function(prevSucc) {
            return prevImageSuccess(prevSucc);
        }, fail);
    }

    // Called on thumbnail creation success
    // Generates the currect URL to the local moviefile
    // Finally resolves the promies and returns the name
    function prevImageSuccess(succ) {
        var correctUrl = succ.slice(0, -4);
        correctUrl += '.MOV';
        deferred.resolve(correctUrl);
    }

    // Called when anything fails
    // Rejects the promise with an Error
    function fail(error) {
        console.log('FAIL: ' + error.code);
        deferred.reject('ERROR');
    }

    // Function to make a unique filename
    function makeid(farm_id) {
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for ( var i=0; i < 5; i++ ) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return farm_id + '-' + text;
    }

    // The object and functions returned from the Service
    return {
        // This is the initial function we call from our controller
        // Gets the videoData and calls the first service function
        // with the local URL of the video and returns the promise
        saveVideo: function(data, farm_id) {
            createFileEntry(data[0].localURL, farm_id);
            return promise;
        }
    }
})
