
var app = angular.module('originsApp', ['ionic', 'originsApp.config', 'originsApp.auth', 'originsApp.storage', 'originsApp.controllers.index', 'originsApp.controllers.farm', 'originsApp.controllers.login', 'originsApp.controllers.settings', 'originsApp.controllers.page', 'originsApp.controllers.postcard', 'originsApp.my_alert', 'ngIOS9UIWebViewPatch', 'angular-carousel'])

.run(function($ionicPlatform, $rootScope) {
	$ionicPlatform.ready(function() {
	    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
	    // for form inputs)

		if (window.cordova && window.cordova.plugins.Keyboard) {
			//cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}

		if (window.plugin && window.plugin.statusbarOverlay) {
			window.plugin.statusbarOverlay.hide();
		}
	});
})

.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

	$httpProvider.defaults.useXDomain = true;
	delete $httpProvider.defaults.headers.common['X-Requested-With'];

	$httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
	$httpProvider.defaults.transformRequest = function(obj) {
		var str = [];
		for(var p in obj)
			str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		return str.join("&");
	}

	/*$httpProvider.interceptors.push(function($rootScope) {
		return {
			request: function(config) {
				$rootScope.$broadcast('loading:show');
				return config;
			},
			'requestError': function(rejection) {
				$rootScope.$broadcast('loading:hide');
				return rejection;
			},
			response: function(response) {
				$rootScope.$broadcast('loading:hide');
				return response;
			},
			'responseError': function(rejection) {
				$rootScope.$broadcast('loading:hide');
				return rejection;
			}
		}
	})*/

	$stateProvider

	.state('index', {
		url:'/',
		templateUrl: 'views/index.html',
		controller: 'IndexController'
	})

	.state('login', {
		url: '/login',
		templateUrl: 'views/login.html',
		controller: 'LoginController'
	})

	.state('farm-index', {
		url: '/farm',
		templateUrl: 'views/farm/index.html',
		controller: 'FarmIndexController'
	})

	.state('farm-save', {
		url: '/farm/save/:farm_id',
		templateUrl: 'views/farm/save.html',
		controller: 'FarmSaveController'
	})

	.state('farm-save.main', {
		url: '/',
		templateUrl: 'views/farm/save.main.html',
		controller: 'FarmSaveController.main'

	})

	.state('farm-save.location', {
		url: '/location',
		templateUrl: 'views/farm/save.location.html',
		controller: 'FarmSaveController.location'
	})

	.state('farm-save.people', {
		url: '/people',
		templateUrl: 'views/farm/save.people.html',
		controller: 'FarmSaveController.people'
	})

	.state('farm-save.size', {
		url: '/size',
		templateUrl: 'views/farm/save.size.html',
		controller: 'FarmSaveController.size'
	})

	.state('farm-save.varieties', {
		url: '/varieties',
		templateUrl: 'views/farm/save.varieties.html',
		controller: 'FarmSaveController.varieties'
	})

	.state('farm-save.processing', {
		url: '/processing',
		templateUrl: 'views/farm/save.processing.html',
		controller: 'FarmSaveController.processing'
	})

	.state('farm-save.legacy', {
		url: '/legacy',
		templateUrl: 'views/farm/save.legacy.html',
		controller: 'FarmSaveController.legacy'
	})

	.state('farm-save.plans', {
		url: '/plans',
		templateUrl: 'views/farm/save.plans.html',
		controller: 'FarmSaveController.plans'
	})

	.state('farm-save.photos', {
		url: '/photos',
		templateUrl: 'views/farm/save.photos.html',
		controller: 'FarmSaveController.photos'
	})

	.state('farm-view', {
		url: '/farm/view/:farm_id',
		templateUrl: 'views/farm/view.html',
		controller: 'FarmViewController'
	})

	.state('settings', {
		url: '/settings',
		templateUrl: 'views/settings.html',
		controller: 'SettingsController'
	})

	.state('postcard', {
		url: '/postcard',
		templateUrl: 'views/postcard/index.html',
		controller: 'PostcardController'
	})

	.state('tools', {
		url: '/tools',
		templateUrl: 'views/tools.html'
	})

	.state('harvest-schedule', {
		url: '/harvest-schedule',
		templateUrl: 'views/harvest-schedule.html',
		controller: 'PageController'
	})

	.state('variety-tree', {
		url: '/variety-tree',
		templateUrl: 'views/variety-tree.html',
		controller: 'PageController'
	})

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/');

})
