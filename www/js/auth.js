angular.module('originsApp.auth', []) 

.service('Auth', function($window, $http, SERVICE_CONFIG, $rootScope, $state, $timeout) {

    this.login = function (credentials) {

        return $http
            .post(SERVICE_CONFIG.base_url+'user/login', credentials)
            .success(function (response) {
                if (response.status == 'success') {
                    
                    $rootScope.user_id = $window.localStorage['user_id'] = response.rec.id;

                    var last_name = response.rec.last_name;
                    if (last_name == 'null') last_name = '';    

                    $rootScope.name = $window.localStorage['name'] = response.rec.first_name +' '+ last_name;
                    $rootScope.username = $window.localStorage['username'] = response.rec.username;
                    $rootScope.first_name = $window.localStorage['first_name'] = response.rec.first_name;
                                   
                    $rootScope.last_name = $window.localStorage['last_name'] = last_name;

                    $rootScope.multi_user = $window.localStorage['multi_user'] = response.rec.multi_user;
                    $rootScope.wifi_sync_only = $window.localStorage['wifi_sync_only'] = response.rec.wifi_sync_only;
                }
            })
            .error(function(response) {

            });
    };

    this.isAuthenticated = function() {
        if (!$window.localStorage['user_id']) return false;
        return true;
    };
    this.logout = function() {
        delete $window.localStorage['user_id'];
        delete $window.localStorage['name'];
        delete $window.localStorage['first_name'];
        delete $window.localStorage['last_name'];
        delete $window.localStorage['multi_user'];
        delete $window.localStorage['wifi_sync_only'];

        $rootScope.user_id = '';
        $rootScope.name = '';
        $rootScope.first_name = '';
        $rootScope.last_name = '';
        $rootScope.multi_user = '';
        $rootScope.wifi_sync_only = '';
        $state.go('index');
    };
    this.user_id = function() {
        return $window.localStorage['user_id'];
    };
    this.name = function() {
        return $window.localStorage['name'];
    };
    this.first_name = function(value) {
        if (value) {
            $rootScope.first_name = $window.localStorage['first_name'] = value;
            $rootScope.name = $window.localStorage['name'] = value +' '+ $window.localStorage['last_name'];
        }
        else return $window.localStorage['first_name'];
    };
    this.last_name = function(value) {
        if (value) {
            $rootScope.last_name = $window.localStorage['last_name'] = value;
            $rootScope.name = $window.localStorage['name'] = $window.localStorage['first_name']+' '+value;
        }
        else return $window.localStorage['last_name'];
    };

    this.username = function(value) {
        if (value) $rootScope.username = $window.localStorage['username'] = value;
        else return $window.localStorage['username'];
    };

    this.multi_user = function(value) {
        if (value != undefined) $rootScope.multi_user = $window.localStorage['multi_user'] = value;
        else if ($window.localStorage['multi_user'] == 1) return true;
        return false;
    };

    this.wifi_sync_only = function(value) {
        if (value != undefined) $rootScope.wifi_sync_only = $window.localStorage['wifi_sync_only'] = value;
        else if ($window.localStorage['wifi_sync_only'] == 1) return true;
        return false;
    };
})