var CanvasText = new CanvasText;

(function(w) {

	var wm = (function(w) {

		var doc = w.document,
		watermark = {},
		gcanvas = {},
		gctx = {},
		config = {};

		this.init = function(options) {

			config = options;

			gcanvas = doc.getElementById('watermark-canvas');
			if (!gcanvas) {
				gcanvas = doc.createElement("canvas");
				gcanvas.setAttribute('id', 'watermark-canvas');
				gcanvas.style.cssText = "display:none;";
				gctx = gcanvas.getContext("2d");
				doc.body.appendChild(gcanvas);
			}
			else {
				gctx = gcanvas.getContext("2d");
				gctx.clearRect(0, 0, gcanvas.width, gcanvas.height);
			}

			watermark = new Image();
			watermark.src = options.watermark;
			watermark.onload = function() {
				var els = doc.getElementsByClassName(options.target),
					len = els.length;
				while (len--) {
					var img = els[len];
					if (img.tagName.toUpperCase() != "IMG")
						continue;

					if (!img.getAttribute('data-original-src')) img.setAttribute('data-original-src', img.src);

					if (!img.complete) {
						img.onload = function() {
							applyWatermark(this);
						};
					} else {
						applyWatermark(img);
					}
				}
			}



		}
		function applyWatermark(img) {

			var new_img = new Image();
			new_img.src = img.getAttribute('data-original-src');
			new_img.onload = function(e) {
				gcanvas.width = new_img.width;
				gcanvas.height = new_img.height;

				gctx.drawImage(new_img, 0, 0);
				gctx.drawImage(watermark, gcanvas.width - watermark.width - 35, gcanvas.height - watermark.height - 35);

				gctx.shadowColor = "black";
				gctx.shadowOffsetX = 5;
				gctx.shadowOffsetY = 5;
				gctx.shadowBlur = 25;

				CanvasText.config({
					canvas:  gcanvas,
					context: gctx,
					fontFamily: "mr_alexlight",
					fontSize: "70px",
					fontWeight: "bold",
					fontColor: "#fff",
					lineHeight: "35"
				});

				CanvasText.defineClass("label", {
					fontSize: "45px",
					fontWeight: "normal"
				});

				CanvasText.defineClass("text", {
					fontSize: "35px",
					fontWeight: "normal"
				});

				var alt = document.getElementById("postcard-alt").value;
				var title = document.getElementById("postcard-title").value;
				var text = '';
				if (alt) text = alt + ' <class="label">masl</class> <br/>';
				text += '<class="text">'+ title +' </class>';

				CanvasText.drawText({
					text: text,
					x: 35,
					boxWidth: gcanvas.width - watermark.width - 35,
					y: gcanvas.height - 105
				});

				img.onload = null;
				img.src = gcanvas.toDataURL();
				document.getElementById("postcard-img").value = img.src;
			}



		}


		return this;
	})(w);

	w.wmark = wm;
})(window);
