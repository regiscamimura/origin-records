app.directive('autocomplete', function($ionicModal, $window, $timeout) {
	return {
		restrict: 'AE',
		replace: 'false',
		scope: {
			src: "=",
			title: "@",
			ngModel: "="
		},
		link: function(scope, elem, attrs) {

			elem.bind('focus', function(e) {
				 e.preventDefault(); 
				 e.stopPropagation();
    			window.scrollTo(0,0);
				scope.openModal();
			});
			elem.attr('readonly');

			scope.rel = scope.$id;

			$ionicModal.fromTemplateUrl('directives/autocomplete/modal.html', {
				scope: scope,
				animation: 'slide-in-up'
			}).then(function(modal) {
				scope.modal = modal;
			})  

			scope.openModal = function() {
				scope.modal.show().then(function() {
					scope.field = $window.document.getElementById('modal-search-'+scope.$id);
					scope.field.focus();
				});				
			}

			scope.closeModal = function() {
				scope.modal.hide();
			};

			scope.$on('$destroy', function() {
				scope.modal.remove();
			});

			scope.selectItem = function(item) {
				scope.field.value = item;
				//scope.ngModel = item;
				//scope.closeModal();
			}

			scope.submit = function() {
				scope.ngModel = scope.field.value;
				scope.closeModal();
			}
		}
	};
});