app.directive('fullscreen', function($ionicModal) {
	return {
		restrict: 'AE',
		replace: 'false',
		scope: {},
		link: function(scope, elem, attrs) {
			
			elem.bind('click', function() {
		        scope.src = elem.attr('src');
		        scope.openModal();
		    });

			$ionicModal.fromTemplateUrl('directives/fullscreen-image/modal.html', {
				scope: scope,
				animation: 'slide-in-up'
			}).then(function(modal) {
				scope.modal = modal;
			})  

			scope.openModal = function() {
				scope.modal.show();
			}

			scope.closeModal = function() {
				scope.modal.hide();
			};

			scope.$on('$destroy', function() {
				//scope.modal.remove();
			});

		}
	};
});